import Router from 'next/router';
import { useEffect } from 'react';

function Index () {

    useEffect(()=>{
        Router.push("/about");
    }, [])
    return <></>
    
}

export default Index;