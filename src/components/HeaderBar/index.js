import * as React from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import MenuIcon from "@mui/icons-material/Menu";

import { IconButton, Typography, Toolbar, AppBar, CssBaseline, useMediaQuery, ListItem, ListItemText, Box } from "@mui/material";
import { Brightness4 as Brightness4Icon, Brightness7 as Brightness7Icon } from "@mui/icons-material";
import { darkModeAtom, sideBarAtom } from "@Components/atoms";
import { useAtom } from "jotai";
import { listMenu } from "@Utilities/mock";

export default function HeaderBar({ title, ...props }) {
	const [, setSideNav] = useAtom(sideBarAtom);
	const [darkMode, setDarkMode] = useAtom(darkModeAtom);
	const isMediaSM = useMediaQuery((theme) => theme.breakpoints.up("sm"));
	const router = useRouter();

	return (
		<>
			<CssBaseline />
			<AppBar>
				<Toolbar>

					<IconButton
						color="inherit"
						aria-label="open drawer"
						onClick={() => setSideNav(true)}
						edge="start"
						sx={{ mr: 2, display: { xs: "none", sm: "block", md: "block", lg: "block" } }}
					>
						<MenuIcon />
					</IconButton>

					<Toolbar style={{ width: "100%", justifyContent: "space-between" }}>
						<Typography variant="h6" component="div">
							{title}
						</Typography>
						<IconButton sx={{ ml: 1 }} onClick={() => setDarkMode(!darkMode)} color="inherit">
							{darkMode ? <Brightness7Icon /> : <Brightness4Icon />}
						</IconButton>
					</Toolbar>
				</Toolbar>
					<Toolbar sx={{ height: 70, overflowY: "none", overflowX: "auto", display: { xs: "flex", sm: "none"} }}>
							{listMenu.map((v, index) => (
								<Link href={v.link} key={v.key} passHref>
									<ListItem button sx={{ borderRadius: 50, textAlign: "center" }}>
										{router.pathname === v.link ? (
											<ListItemText primaryTypographyProps={{ fontWeight: "bold" }} primary={v.text} />
										) : (
											<ListItemText primary={v.text} />
										)}
									</ListItem>
								</Link>
							))}

					</Toolbar>
			</AppBar>
			<Toolbar />
			{!isMediaSM && <><Box sx={{ marginY : 10}} /></>}
		</>
	);
}
