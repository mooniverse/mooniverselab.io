import { useEffect, useState } from "react";
import { useAtom } from "jotai";

import { SideBar, HeaderBar, Footer } from "@Components/index";
import { darkModeAtom, sideBarAtom } from "@Components/atoms";

import { CircularProgress, Typography, Box, Container, Grid, Button, Icon, Divider, IconButton, useMediaQuery } from "@mui/material";
import { listBio, listTag, listContact } from "@Utilities/mock";
import Image from "next/image";
import Axios from "axios";

function About() {
	const [, setSideNav] = useAtom(sideBarAtom);
	const [darkMode] = useAtom(darkModeAtom);
	const [dataWiki, setDataWiki] = useState();

	const [showLoadingAPI, setShowLoadingAPI] = useState(false);

	const isMediaSM = useMediaQuery((theme) => theme.breakpoints.up("sm"));

	const searchApiWikipedia = (searchString) => {
		setDataWiki();
		setShowLoadingAPI(true);
		const configApiWiki = {
			method: "GET",
			url: "https://en.wikipedia.org/w/api.php",
			params: {
				origin: "*",
				format: "json",
				action: "query",
				prop: "extracts",
				exintro: "",
				explaintext: "",
				redirects: 1,
				titles: searchString,
			},
		};
		Axios(configApiWiki)
			.then((response) => {
				const responseWiki = Object.values(response.data?.query.pages || {})?.[0];
				if (responseWiki?.extract && responseWiki?.title) {
					setDataWiki({
						title: responseWiki.title,
						data: responseWiki.extract,
					});
				} else {
					setDataWiki({
						title: searchString,
						data: "Sorry, no data found",
					});
				}
			})
			.catch(() => {
				setDataWiki({
					title: searchString,
					data: "Sorry, wikipedia not available right now :(",
				});
			})
			.finally(() => {
				setShowLoadingAPI(false);
			});
	};

	useEffect(() => {
		setSideNav(false);
	}, []);

	return (
		<>
			<HeaderBar title="About Me" />
			<SideBar />
			<Container sx={{ py: 2 }}>
				<Container
					sx={{
						borderRadius: 4,
						py: 2,
						bgcolor: "secondary.main",
						boxShadow: 4,
					}}
				>
					<Container>
						<Box
							src="/assets/images/profile.jpg"
							xs={3}
							sx={{
								display: { xs: "none", sm: "none", md: "block", lg: "block" },
								borderRadius: 6,
								height: 500,
								backgroundColor: "common.black",
								alignContent: "center",
								textAlign: "center",
								"&:hover": {
									backgroundColor: "common.black",
									opacity: [0.9, 0.8, 0.7],
								},
							}}
						>
							<Image src="/assets/images/profile.jpg" height="500" width="600" />
						</Box>

						<Box sx={{ my: 4, textAlign: "center" }}>
							<Typography variant="h5">Biography</Typography>
							<Grid container sx={{ textAlign: "center", justifyContent: "center" }}>
								{listBio.map((v) => (
									<Grid
										item
										key={v.text}
										sx={{
											my: 1,
											justifyItems: "center",
											textAlign: "center",
										}}
										xs={isMediaSM ? 3 : 5}
									>
										<Icon>{v.icon}</Icon>
										<Typography sx={{ marginLeft:0.5 }} display="inline">{v.text}</Typography>
									</Grid>
								))}
							</Grid>
						</Box>

						<Divider />

						<Box sx={{ my: 4, alignContent: "center", textAlign: "center" }}>
							<Typography variant="h5">About Me</Typography>
							<Typography variant="overline">Hi guys, i'm erry. Nice to meet you!</Typography>

							<Grid container sx={{ justifyContent: "center" }}>
								{listContact.map((v, i) => (
									<Grid item key={v.title} sx={{ mt: 1 }} display="inline">
										<Button
											component="a"
											href={v.url}
											target="_blank"
											variant={ darkMode ? "outlined" : "contained" }
											sx={
												darkMode ?
												{
													mr: 1,
													borderRadius: 6,
													color: v.color,
													borderColor: v.color,
													":hover": {
														color: v.color,
														borderColor: v.color
													}
												}
												:
												{
													mr: 1,
													borderRadius: 6,
													backgroundColor: v.color,
													":hover": {
														backgroundColor: v.color,
														opacity: [0.9, 0.9, 0.9],
													}
												}
											}
										>
											{v.icon}
											{v.value}
										</Button>
									</Grid>
								))}
							</Grid>
						</Box>

						<Divider />

						<Box sx={{ my: 4, alignContent: "center", textAlign: "center" }}>
							<Typography variant="h5">My Tags</Typography>
							<Typography variant="overline">Click For Explanation From Wikipedia API</Typography>
							<Grid container>
								{listTag.map((v) => (
									<Grid item key={v.text} sx={{ mt: 1 }} display="inline">
										<Button
											variant={darkMode ? "outlined" : "contained"}
											onClick={() => searchApiWikipedia(v.text)}
											sx={
												darkMode
													? {
															mr: 2,
															color: v.color,
															borderColor: v.color,
															borderRadius: 6,
															":hover": {
																color: v.color,
																borderColor: v.color
															}
													  }
													: {
															mr: 2,
															backgroundColor: v.color,
															borderRadius: 6,
															":hover": {
																backgroundColor: v.color,
																opacity: [0.9, 0.9, 0.9],
															}
													  }
											}
										>
											{v.text}
										</Button>
									</Grid>
								))}
							</Grid>
						</Box>

						<Divider />

						<Box sx={{ my: 4 }}>
							{showLoadingAPI && (
								<Box sx={{ width: "100%", textAlign: "center" }}>
									<CircularProgress sx={{ position: "relative", zIndex: 2 }} />
								</Box>
							)}
							{dataWiki && (
								<>
									<Typography variant="h5">{dataWiki.title}</Typography>
									<Typography variant="subtitle1">{dataWiki.data}</Typography>
								</>
							)}
						</Box>

						{dataWiki && <Divider />}


					</Container>
				</Container>

				<Container
					sx={{
						marginY: 2,
						borderRadius: 4,
						py: 2,
						bgcolor: "secondary.main",
						boxShadow: 4,
					}}
				>
					<Footer />
				</Container>

			</Container>
		</>
	);
}

export default About;
