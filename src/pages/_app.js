import React, { useEffect } from "react";
import Head from "next/head";
import { ThemeProvider, CssBaseline } from "@mui/material";

import { useAtom } from "jotai";
import { darkModeAtom } from "@Components/atoms";

import LightTheme from "@Styles/theme/LightTheme";
import DarkTheme from "@Styles/theme/DarkTheme";

const MyApp = (props) => {
	const { Component, pageProps } = props;
	const [darkMode] = useAtom(darkModeAtom);

	return (
		<>
			<Head>
				<meta charSet="UTF-8" />
				<meta name="viewport" content="width=device-width, initial-scale=1.0" />
				<meta name="description" content="Mooniverse Page" />
				<meta name="author" content="erryjuliox@gmail.com" />
				<meta name="keywords" content="profile mooniverse portofolio"></meta>
				<title>Pages · Mooniverse</title>
			</Head>
			
			<ThemeProvider theme={darkMode ? DarkTheme : LightTheme}>
				<CssBaseline />
				<Component {...pageProps} />
			</ThemeProvider>
			
		</>
	);
};

export default MyApp;
