import * as React from 'react';
import Document, { Html, Head, Main, NextScript } from 'next/document';

import { Box } from "@mui/material"

class MyDocument extends Document {
    render() {
        return (
            <Html lang='en'>
                <Head>
                    <link rel="shortcut icon" type="image/x-icon" href="/assets/images/brand/favicon.ico" />    
                </Head>
                <Box component="body">
                    <NextScript />
                    <Main />
                </Box>
            </Html>
        )
    }
}

export default MyDocument