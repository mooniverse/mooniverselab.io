import {
	Badge as BadgeIcon,
	Cake as CakeIcon,
	LocationOn as LocationIcon,
	Work as WorkIcon,
	School as SchoolIcon,
	MenuBook as BookIcon,
	Instagram as InstagramIcon,
	WhatsApp as WhatsAppIcon,
	Person as PersonIcon,
	Feed as FeedIcon,
	Email as EmailIcon
} from "@mui/icons-material";
import { green, grey, purple, red } from "@mui/material/colors";

export const listMenu = [
	{
		key: "About",
		text: "About",
		link: "/about",
		icon: <PersonIcon />
	},
	{
		key: "Portofolio",
		text: "Portofolio",
		link: "/portofolio",
		icon: <FeedIcon />
	},
	{
		key: "Activities",
		text: "Activities",
		link: "/activities",
		icon: <FeedIcon />
	},
	{
		key: "Timeline",
		text: "Timeline",
		link: "/timeline",
		icon: <FeedIcon />
	},
	{
		key: "Other",
		text: "Other",
		link: "/other",
		icon: <FeedIcon />
	},
]

export const listContact = [
	{
		title: "Instagram",
		icon: <InstagramIcon />,
		value: "erryjulio",
		url: "https://www.instagram.com/erryjulio/",
        color: "primary.dark"
	},
	{
		title: "WhatsApp",
		icon: <WhatsAppIcon />,
		value: "+6285156795395",
		url: "https://wa.me/6285156795395",
        color: green[400]
	},
	{
		title: "Email",
		icon: <EmailIcon />,
		value: "erryjuliox@gmail.com",
		url: "mailto:erryjuliox@gmail.com?Subject=Hi Erry",
        color: red[400]
	},
];

export const listBio = [
	{
		icon: <BadgeIcon />,
		text: "Erry Julio",
	},
	{
		icon: <CakeIcon />,
		text: `1999-${new Date().getFullYear()} (${new Date(new Date() - new Date("1999-07-16")).getUTCFullYear() - 1970})`,
	},
	{
		icon: <LocationIcon />,
		text: "Tangerang / Indonesia",
	},
	{
		icon: <WorkIcon />,
		text: "Fullstack Developer",
	},
	{
		icon: <SchoolIcon />,
		text: "UKSW",
	},
	{
		icon: <BookIcon />,
		text: "Informatic Engineering (S1)",
	},
];

export const listTag = [
	{ text: "Programming", color: "warning.main" },
	{ text: "Node JS", color: "success.main" },
	{ text: "Javascript", color: "warning.main" },
	{ text: "Typescript" },
	{ text: "Express JS", color: "warning.main" },
	{ text: "React Native" },
	{ text: "Next JS", color: grey[500] },
	{ text: "Sequelize" },
	{ text: "PHP", color: purple[500] },
	{ text: "Database", color: "warning.main" },
	{ text: "SQL", color: "error.main" },
	{ text: "PostgreSQL" },
	{ text: "MongoDB", color: "error.main" },
	{ text: "MySQL", color: "success.main" },
	{ text: "Infrastructure", color: "success.dark" },
	{ text: "Docker" },
	{ text: "Git", color: "warning.dark" },
	{ text: "Cloud", color: "success.dark" },
	{ text: "Linux", color: purple[400] },
	{ text: "Bash" },
	{ text: "Backend" },
	{ text: "Frontend" },
	{ text: "Fullstack", color: grey[400] },
	{ text: "Android", color: "success.main" },
	{ text: "Server" },
	{ text: "API", color: "error.dark" },
	{ text: "Software", color: "warning.dark" },
	{ text: "Developer" },
	{ text: "Computer", color: "error.main" },
	{ text: "Photoshop", color: "primary.dark" },
];
