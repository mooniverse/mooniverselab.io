This is a starter template for [Learn Next.js](https://nextjs.org/learn).

Deployment with gitlab runner
```yml
image: node

pages:
  stage: build
  script:
    - yarn
    - yarn build
    - yarn export
    - rm -rf public/*
    - mv out/* public
  artifacts:
    paths:
      - public
  cache:
    paths:
      - node_modules
  only:
    - master
```