import { createTheme } from '@mui/material/styles';
import { grey } from '@mui/material/colors';

const DarkTheme = createTheme({
  palette: {
    mode: 'dark',
    secondary: {
      main: grey[900],
    },
  },
});

export default DarkTheme;