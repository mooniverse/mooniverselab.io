import { atom } from 'jotai';
import { atomWithStorage } from 'jotai/utils';

export const sideBarAtom = atom(false);
export const darkModeAtom = atom(false);

