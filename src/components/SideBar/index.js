import React from "react";
import { Drawer, Box, List, Divider, ListItem, ListItemText, ListItemIcon } from "@mui/material";
import { sideBarAtom } from "@Components/atoms";
import { listMenu } from "@Utilities/mock"
import { useAtom } from "jotai";
import { styled } from '@mui/material/styles';
import Link from "next/link";

function SideBar ({width = 250}) {
    const [sideNav, setSideNav] = useAtom(sideBarAtom);

    const DrawerHeader = styled('div')(({ theme }) => ({
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-start',
        padding: theme.spacing(0, 1),
        // necessary for content to be below app bar
        ...theme.mixins.toolbar,
    }));

    return (
        <>
            <Drawer
                anchor="left"
                open={sideNav}
                onClose={()=>setSideNav(false)}
            >
                <Box
                    sx= { { width, } }
                    role="presentation"
                >
                    <DrawerHeader style={ { justifyContent:"center" } }>
                        I'm Mooniverse
                    </DrawerHeader>
                    <List>
                        {listMenu.map((v, index) => (
                            <Link href={v.link} key={v.key} passHref>
                                <ListItem button key={v.key}>
                                    <ListItemIcon>
                                        {v.icon}
                                    </ListItemIcon>
                                    <ListItemText primary={v.text} />
                                </ListItem>
                            </Link>
                        ))}
                    </List>
                    <Divider />
                </Box>
            </Drawer>
        </>
    )
    
}

export default SideBar;