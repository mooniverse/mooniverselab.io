FROM node:14-alpine

WORKDIR /usr/src/app

COPY package*.json ./

RUN ln -sf /usr/share/zoneinfo/Asia/Jakarta /etc/localtime

RUN npm install

COPY . .

RUN npm run build

CMD "npm" "run" "start"
