import { Container, Typography } from "@mui/material";
import { grey } from "@mui/material/colors";


function Footer () {

    return (
        <Container sx={{ my:2, color: grey[500], display: "flex", flexDirection:"column", textAlign: "center"}}>
            <Typography variant="subtitle2"> Copyright © {new Date().getFullYear()} Erry Julio {"<"}erryjuliox{"@"}gmail.com{">"} </Typography>
            <Typography variant="subtitle2"> Powered with Next.JS </Typography>
        </Container>
    )
}

export default Footer;