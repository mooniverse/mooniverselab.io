import axios from "axios";

const Get = (url, params, headers) => {
    return axios({
        method: "GET",
        url,
        headers,
        params,
    }).then(
        (response) => (response?.data) ? response.data : null
    )
};
const Post = (url, body, params, headers) => {
    return axios({
        method: "POST",
        url,
        body,
        params,
        headers,
        params,
    }).then(
        (response) => (response?.data) ? response.data : null
    )
};
const Put = (url, body, params, headers) => {
    return axios({
        method: "PUT",
        url,
        body,
        params,
        headers,
        params,
    }).then(
        (response) => (response?.data) ? response.data : null
    )
};
const Delete = (url, body, params, headers) => {
    return axios({
        method: "DELETE",
        url,
        body,
        params,
        headers,
        params,
    }).then(
        (response) => (response?.data) ? response.data : null
    )
};

const Services = {
	Get,
	Post,
	Put,
	Delete,
};

export default Services;
