import { useEffect } from "react";
import { useAtom } from "jotai";

import { SideBar, HeaderBar } from "@Components/index";
import { sideBarAtom } from "@Components/atoms";

import { Typography } from "@mui/material"

function Portofolio () {

    const [,setSideNav] = useAtom(sideBarAtom);

    useEffect( () => {
        
        setSideNav(false);
        
    }, []);

    return (
        <>
            <HeaderBar title="My Portofolio" />
            <SideBar />
            <Typography>
                Coming Soon!
            </Typography>
        </>
    )
    
}

export default Portofolio;